--module HW1
  --     ( hw1_1
    --   , hw1_2
      -- , fact2
       --, isPrime
       --, primeSum
       --) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты


-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a+b

-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 n = fromIntegral n

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 n = if n>0
          then n*fact2(n-2)
		  else 1
		

fact n = if n==0
         then 1 
		 else n * fact(n-1)

-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
isPrime :: Integer -> Bool
isPrime p = if p == 1
            then False
            else if (fromIntegral((fact(p-1)+1) `mod` p) == 0) then True else False		

                 				  
-- |Найти сумму всех простых чисел в диапазоне [a;b]
primeSum :: Integer -> Integer -> Integer
primeSum a b = if a <= b 
               then if isPrime(a)
 			        then a+primeSum (a+1)  b
					else primeSum (a+1)  b
			   else 0

