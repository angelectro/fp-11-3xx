{-==== ФАМИЛИЯ ИМЯ, НОМЕР ГРУППЫ ====-}

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}
rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket xs = if ((min' [maximum xs]))==(min' xs) then [maximum xs] else xs

maximum' :: [(Integer, Integer)] -> (Integer, Integer)
maximum' [] = error "maximum of empty list"  
maximum' [(a, b)] = (a, b)
maximum' ((a,b):xs) = maxTail (a,b) (maximum' xs) 
    where maxTail :: (Integer, Integer) -> (Integer, Integer) -> (Integer, Integer)
          maxTail (a,b) (c,d) 
           | (quot a b) > (quot c d) = (a,b) 
           | (quot a b) < (quot c d) = (c,d) 
           | (quot a b) == (quot c d) = (a,b) 

min' :: [(Integer, Integer)] -> Integer
min' [] = error "maximum of empty list"  
min' [(a, b)] = b
min' ((a,b):xs) = minTail b (min' xs) 
    where minTail :: Integer -> Integer -> Integer
          minTail b d 
           | b > d = d 
           | b < d = b
           | otherwise = b



{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Ord,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

[Orbiter "ISS", Orbiter "LRO"]
orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
 -}
orbiters :: [Spaceship a] -> [Load a]
orbiters [] = []
orbiters ((Rocket d x) : xs) = merge (orbiters x) (orbiters xs)
orbiters ((Cargo x) : xs) =  merge (filter x) (orbiters xs)
           where filter :: [Load a] -> [Load a] 
                 filter ((Orbiter f):fs) = Orbiter f : filter fs
                 filter ((Probe f):fs) =  filter fs
                 filter [] = []

merge :: [Load a] -> [Load a] -> [Load a]
merge xs     []     = xs
merge []     ys     = ys
merge (x:xs) (y:ys) = x : y : merge xs ys

{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
finalFrontier a = map getText a 


getText :: Phrase -> String 
getText (Warp c) = "Kirk" 
getText (BeamUp s) = "Kirk" 
getText (IsDead s) = "McCoy" 
getText (LiveLongAndProsper) = "Spock" 
getText (Fascinating) = "Spock"
