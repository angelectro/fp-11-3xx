import Data.List
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as S
import qualified Data.Map as M
 	


limit = 12000


fillMap:: Map Integer Integer
fillMap = map 1
   where 
   	map :: Integer -> Map Integer Integer
   	map  l = if (l<=(limit+1)) then 
   		                      M.insert l (limit*2) (map(l+1))
                             else M.empty


factorize ::Map Integer Integer ->Integer ->Integer->Integer->Integer->Integer->Map Integer Integer
factorize map n remain maxFactor sum terms = if(remain==1) then
	                                                              if(ter <=limit) then
	                                                     	        replaceMap ( M.findIndex ter map)
	                                                     	       else map
	                                                           else
	                                                             eval map maxFactor 
	               where 
	               	eval:: Map Integer Integer ->Integer->Map Integer Integer
	               	eval m maxF  = if(maxF>=2)then
	               		                  if((remain `mod` maxF)==0)then
	               		              	   eval (factorize m n (round $ ((fromIntegral(remain))/ fromIntegral(maxF))) (min maxF maxFactor) (sum+maxF) (terms+1)) (maxF-1)
	               		                  else
	               		                 	 eval m (maxF-1)
	               		               else
	               		         	    m
	                ter=terms+n-sum
	                replaceMap::Int->Map Integer Integer
	                replaceMap t = if(snd(M.elemAt t map)>n)then
	                	                                    M.insert ter n (M.deleteAt t map) 
	                	                                  else
	                	                                   	map

set'::[Integer] -> Set Integer 
set' (l:ls) = S.insert l (set' ls)
set' [] =  S.empty


problem88 :: Integer -> Map Integer Integer -> Integer
problem88 l s= if(l<=(limit*2)) then 
	                             problem88 (l+1) (factorize s l l l 0 0)
	                             else
	                             	 sum' (S.elems (set'(M.elems(M.deleteAt (M.findIndex (limit+1) s) s))))
	                           where 
	                           	sum'::[Integer] ->Integer
	                           	sum' (l:ls) =l+(sum' ls)
	                           	sum' [] = -2
                                
                             
