-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown c = case c of
  Off -> True
  On -> True
  Unknown -> False

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
eval (Const n) = n
eval (Add a b) = eval a + eval b
eval (Sub a b) = eval a - eval b
eval (Mult a b) = eval a * eval b

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify t = if ((doSimplify t) == t) then t else (simplify (doSimplify t))
 
doSimplify :: Term -> Term
doSimplify t = case t of
  (Mult (Add a b) c) -> Add (Mult (simplify a) (simplify c)) (Mult (simplify b) (simplify c))
  (Mult c (Add a b)) -> Add (Mult (simplify a) (simplify c)) (Mult (simplify b) (simplify c))
  (Mult (Sub a b) c) -> Sub (Mult (simplify a) (simplify c)) (Mult (simplify b) (simplify c))
  (Mult c (Sub a b)) -> Sub (Mult (simplify a) (simplify c)) (Mult (simplify b) (simplify c))
  (Mult a b) -> Mult (simplify a) (simplify b)
  (Add a b) -> Add (simplify a) (simplify b)
  (Sub a b) -> Sub (simplify a) (simplify b) 
  _ -> t

