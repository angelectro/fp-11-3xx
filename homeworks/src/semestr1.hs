module SEMESTR1
       ( eval1,
       Term (..),
       replaceVars
       ) where

data Term = Var String
            |Lambda Term Term
            |Apply Term Term
            deriving(Eq)
instance Show Term where
    show (Var v) = v
    show (Lambda v t) = "(\\" ++ show v ++ " . " ++ show t ++ ")"
    show (Apply t1 t2) = show t1 ++ " " ++ show t2

eval1 :: Term -> Maybe Term
eval1 (Var x) = Just (Var x)
eval1 (Apply (Var x) (Var y))=Just (Apply (Var x) (Var y))
eval1 (Apply (Lambda (Var x) y) t) =  case result of
      (Apply x y) -> eval1 (Apply x y)
      otherwise -> Just result
      where
        result = (replaceVars (Var x) t y)
eval1 (Apply (Apply (Lambda (Var x) (Lambda y z)) t) m) = eval1 $ Apply (Lambda y (replaceVars (Var x) t z)) m
eval1 (Apply (Lambda x y) t) = Just $ replaceVars x t y
eval1 t = Nothing

eval :: Term -> Term
eval x = case result of
  (Just x) -> x
  otherwise -> Var "Not evaulated"
  where
    result = eval1 x
replaceVars :: Term -> Term -> Term -> Term
replaceVars (Var x) t (Var y) = if x==y then t else  (Var y)
replaceVars (Var x) t (Lambda y z) = Lambda y (replaceVars (Var x) t z)
replaceVars (Var x) t (Apply y z) = Apply (replaceVars (Var x) t y) (replaceVars (Var x) t z)