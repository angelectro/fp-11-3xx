

primes :: [Integer]
primes = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97]

limit = 10^9

getPrime::[Integer] -> Integer-> Integer
getPrime (l:ls) n = if(n==0)  then l
                               else  getPrime ls (n-1)
getPrime [] n = 0


count:: Integer -> Integer -> Integer
count primeIndex product = if(primeIndex==25) then
                                                     if(product<=limit) then
                                                                          1
                                                                          else
                                                                            0
                                                     else
                                                       comb product primeIndex

comb :: Integer -> Integer-> Integer
comb pr primeIndex = if(pr<=limit) then 
                                            ((count (primeIndex+1) pr)+(comb ((getPrime  primes primeIndex)*pr) primeIndex))
                                            else 0
